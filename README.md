# CodingChallenge

Small webapp that will list the most starred Github repos that were created in the last 30 days. 



## Features

* As a User I should be able to list the most starred Github repos that were created in the last 30 days.
* As a User I should see the results as a list. One repository per row.
* As a User I should be able to see for each repo.

*  As a User I should be able to keep scrolling and new results should appear (pagination).

## Technologies 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.2.

### Libraries We Use

    
*  Moment.js => to manipulate the date and time in our webapp.(https://momentjs.com/)
    
    
*  Angular Infinite Scroll => to implement the pagination while scrolling a web page. (https://www.npmjs.com/package/ngx-infinite-scroll)

## How to run
*  Navigate to the project folder.
*  Run `npm install` command to install all the dependencies.
*  Run `ng serve` command.
*  Navigate to `http://localhost:4200/`.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma].


