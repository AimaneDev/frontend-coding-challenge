import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GithubReposListComponent } from './components/github-repos-list/github-repos-list.component';

const routes: Routes = [
  {
    path: '' ,
    component : GithubReposListComponent,

  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
