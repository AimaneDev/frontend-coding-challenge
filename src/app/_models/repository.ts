export interface Repository {
    id          : Number ;
    name   : string ;
    description    : string ;
    stargazers_count  : Number ;
    open_issues_count: Number ;
    created_at : string ;
    owner : owner ;
}

interface owner {
    login : string ;
    avatar_url : string;
}