import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShortNumberPipe } from './short-number.pipe';
import { DaysFromNowPipe } from './days-up-to-now.pipe';

@NgModule({
  declarations: [
    ShortNumberPipe,
    DaysFromNowPipe
  ],
  imports: [
    CommonModule
  ],
  exports : [
    ShortNumberPipe,
    DaysFromNowPipe
  ]
})
export class SharedModule { }
