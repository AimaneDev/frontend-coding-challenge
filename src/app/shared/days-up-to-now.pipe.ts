import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({ name: 'daysUpToNow' })
export class DaysFromNowPipe implements PipeTransform {
    transform(date: number, exponent: string): number {
        // const days = moment(date, "YYYY-MM-DD").fromNow();
        const days = moment().diff(date, 'days');

        return days
    }
}