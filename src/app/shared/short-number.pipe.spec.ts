import { ShortNumberPipe } from './short-number.pipe';

describe('ShortNumberPipe', () => {
  it('create an instance', () => {
    const pipe = new ShortNumberPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return a short number', ()=> {
    const pipe = new ShortNumberPipe();
    const shortNumber = pipe.transform(100000, null);
    expect(shortNumber).toEqual('100K');
  })
});