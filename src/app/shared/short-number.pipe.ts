import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'shortNumber' })
export class ShortNumberPipe implements PipeTransform {
    transform(value: number, exponent: string): number | string {
        if (isNaN(+value))
            return 0
        else {
            // hundreds
            if (value <= 999) {
                return value;
            }
            // thousands
            else if (value >= 1000 && value <= 999999) {
                return (value / 1000).toFixed(0) + 'K';
            }
            // millions
            else if (value >= 1000000 && value <= 999999999) {
                return (value / 1000000).toFixed(0) + 'M';
            }
            // billions
            else if (value >= 1000000000 && value <= 999999999999) {
                return (value / 1000000000).toFixed(0) + 'B';
            }
        }
    }
}