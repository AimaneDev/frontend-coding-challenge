import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GithubReposListComponent } from './github-repos-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { GithubReposService } from 'src/app/_services/github-repos.service';
import { HttpClientTestingModule  } from '@angular/common/http/testing';
import { from } from 'rxjs';

describe('GithubReposListComponent', () => {
  let component: GithubReposListComponent;
  let fixture: ComponentFixture<GithubReposListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [HttpClientTestingModule, SharedModule],
      declarations: [ GithubReposListComponent ],
      providers : [ GithubReposService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GithubReposListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    let service = TestBed.get(GithubReposService);

    let repositories = [
      {
        items: [
          {
            created_at: "2019-03-27T23:53:37Z",
            description: "description",
            id: 178092134,
            name: "coding challenge",
            open_issues_count: 133,
            owner: {
              avatar_url: "https://avatars0.githubusercontent.com/u/1050213?v=4",
              login: "formulahendry"

            },
            stargazers_count: 15691
          }
        ]
      }
    ];

    spyOn(service, 'loadRepositories').and.returnValue(from(repositories));

    component.ngOnInit();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set repositories proprety with the items returned from github api' , () => {
    expect(component.repositories.length).toBe(1);
  })

  it('html should render one repository', (() => {
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('h3');
    expect(el.innerText).toContain('coding challenge');
  }));
});
