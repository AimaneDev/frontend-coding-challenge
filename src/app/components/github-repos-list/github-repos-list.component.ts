import { Component, OnInit, OnDestroy } from '@angular/core';
import { GithubReposService } from 'src/app/_services/github-repos.service';
import { takeWhile } from 'rxjs/operators';
import * as moment from 'moment';
import { Repository } from 'src/app/_models/repository';


@Component({
  selector: 'app-github-repos-list',
  templateUrl: './github-repos-list.component.html',
  styleUrls: ['./github-repos-list.component.scss']
})
export class GithubReposListComponent implements OnInit, OnDestroy {
  private alive : boolean = true;
  public repositories : Repository[] = [];
  private page: number = 1;  

  constructor(private githubReposService : GithubReposService) { }

  ngOnInit() {
    this.loadRepositories();
  }

  loadRepositories(){
    //GET DATE 30 DAYS BEFORE CURRENT DATE
    const created = moment().subtract(30, 'day').format("YYYY-MM-DD");

    this.githubReposService.loadRepositories(created , 'stars' , 'desc', this.page)
      .pipe(takeWhile(() => this.alive))
      .subscribe((res : any) => {
        if(res)
          this.repositories.push(...res.items as Repository[]) ; 
        // console.log(this.repositories);
      },
      err => console.log(err)
      )
  }

  // TRIGGERS WHEN SCROLL DOWN THE SCREEN  
  onScroll()  
  {  
    console.log("Scrolled");  
    this.page = this.page + 1;  
    this.loadRepositories();  
  } 
 
  ngOnDestroy(){
    this.alive = false;
  }


}
