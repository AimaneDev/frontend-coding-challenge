import { TestBed } from '@angular/core/testing';

import { GithubReposService } from './github-repos.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('GithubReposService', () => {
  beforeEach(() => TestBed.configureTestingModule({      
    imports : [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: GithubReposService = TestBed.get(GithubReposService);
    expect(service).toBeTruthy();
  });
});
