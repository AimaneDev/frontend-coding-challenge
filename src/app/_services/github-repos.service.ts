import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GithubReposService {

  constructor(private http: HttpClient) { }

  loadRepositories(created : string , sortBy : string , orderBy : string, page: number) {
    return this.http.get<any[]>('https://api.github.com/search/repositories?q=created:>='+ created +'&sort=' +sortBy+ '&order='+ orderBy +'&page='+ page)
    .pipe(
      retry(1),
      catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert('Error : Could not load data !');
    return throwError(errorMessage);
  }



}
